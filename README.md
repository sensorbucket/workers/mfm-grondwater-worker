# mfm-grondwater-worker

The MFM Grondwater Worker offers a ZMQ Router to which processed messages can be pushed. Messages are processed, enriched with device data from the asset manager and then send over a ZMQ connection for further processing

For the MFM Grondwater Worker to function a few environment variables need to be set:

- WORKER_ZMQ_HOST, the address on which ZMQ messages should be received for processing
- WORKER_ZMQ_ENDPOINT, the ZMQ host which will receive the processed messages from the MFM Grondwater Worker
- WORKER_LWAM_ENDPOINT, the asset manager's HTTP endpoint on which to retrieve device data