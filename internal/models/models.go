package models

import (
	"encoding/base64"
	"fmt"

	"sensorbucket.nl/sensorbucket/pkg/pipeline"
)

const (
	MeasurementUnitMilivolt    = "millivolt"
	MeasurementUnitCentimeters = "centimeters"
)

type MFMGrondWater pipeline.Device
type Sensor struct {
	pipeline.Sensor
	MeasurementValue MeasurementValue `json:"-,omitempty"`
}

func (mfm *MFMGrondWater) SensorsWithMeasurements(timestamp int64, measurements [][]byte) ([]Sensor, error) {
	//if len(mfm.Sensors) != len(measurements) {
	//	return nil, fmt.Errorf("expected amount of sensors to be the same as measurements but got '%d' sensors and '%d' measurements", len(mfm.Sensors), len(measurements))
	//}
	sensors := []Sensor{}
	for i, meas := range measurements {
		// external id of the sensor is linked to the order in which measurements have arrived
		for _, sens := range mfm.Sensors {
			if *sens.ExternalID == fmt.Sprint(i) {
				sensors = append(sensors, Sensor{
					Sensor: pipeline.Sensor{},
					MeasurementValue: MeasurementValue{
						b:         meas,
						Timestamp: timestamp,
					},
				})
				break
			}
		}
	}
	return sensors, nil
}

func (s *Sensor) Measurements() ([]pipeline.Measurement, error) {
	measurements := []pipeline.Measurement{}
	measurements = append(measurements, pipeline.Measurement{
		Value:             s.MeasurementValue.ToMillivolt(),
		MeasurementTypeID: MeasurementUnitMilivolt,
		Timestamp:         s.MeasurementValue.Timestamp,
		Metadata:          make(map[string]any),
		SensorCode:        s.Code,
	})
	measurements = append(measurements, pipeline.Measurement{
		Value:             s.MeasurementValue.ToCentimeters(),
		MeasurementTypeID: MeasurementUnitCentimeters,
		Timestamp:         s.MeasurementValue.Timestamp,
		Metadata:          make(map[string]any),
		SensorCode:        s.Code,
	})

	return measurements, nil
}

type Payload string

func (payload *Payload) Decode() ([][]byte, error) {
	bytes, _ := base64.StdEncoding.DecodeString(string(*payload))
	if len(bytes) == 0 {
		return nil, nil
	}

	if bytes[0] != 0x6c || bytes[1] != 0x11 {
		return nil, fmt.Errorf("not a proper device measurement")
	}

	sensor1Data := bytes[2:5]
	hasTwo := sensor1Data[0]&0x80 == 0x80
	sensor1Data[0] = sensor1Data[0] & 0x7F
	res := [][]byte{sensor1Data}
	if hasTwo {
		res = append(res, bytes[5:8])
	}

	return res, nil
}

type MeasurementValue struct {
	b         []byte
	Timestamp int64
}

func (meas *MeasurementValue) ToMillivolt() float64 {
	m := *meas
	if len([]byte(m.b)) < 3 {
		panic("cannot decode millivolts")
	}
	b := []byte(m.b)
	return float64((uint32(b[0])<<16)|(uint32(b[1])<<8)|uint32(b[2])) / 100
}

func (meas *MeasurementValue) ToCentimeters() float64 {
	m := *meas
	if len([]byte(m.b)) < 3 {
		panic("cannot decode centimeters")
	}

	b := []byte(m.b)

	// Process value
	proc := float64((uint32(b[0])<<16 | uint32(b[1])<<8 | uint32(b[2]))) / 100.0

	// Convert to centimeters
	return (proc/1000.0 + 0.00702) / 0.00975
}
