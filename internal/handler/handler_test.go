package handler

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Handle(t *testing.T) {
	h := Handler{}
	for _, data := range testData {
		topic, res, err := h.Handle(data.input)
		assert.Nil(t, err)
		//assert.Equal(t, data.expected, string(res))
		fmt.Println("RESULT", string(res))
		assert.Equal(t, "next_queue", topic)
	}
}

type testCase struct {
	input    []byte
	expected []byte
}

var testData = []testCase{
	{
		input: []byte(`{"message_id":"blabla","pipeline_id":"asd","pipeline_steps":["next_queue"],"device":{
			"sensors": [
				{
					"code": "BLABLA",
					"external_id": "0"
				}
			]
		},"timestamp":1663515220,"measurements":[{"value":-117,"properties":{"gw_eui":"FCC23DFFFE2DF1CA","gw_id":"skyhigh-goes"},"measurement_type_id":"rssi"},{"value":0.25,"properties":{"gw_eui":"FCC23DFFFE2DF1CA","gw_id":"skyhigh-goes"},"measurement_type_id":"snr"},{"value":-104,"properties":{"gw_eui":"FCC23DFFFE0AA792","gw_id":"zierikzee"},"measurement_type_id":"rssi"},{"value":6.25,"properties":{"gw_eui":"FCC23DFFFE0AA792","gw_id":"zierikzee"},"measurement_type_id":"snr"},{"value":-113,"properties":{"gw_eui":"FCC23DFFFE0AA6E3","gw_id":"goes"},"measurement_type_id":"rssi"},{"value":0.8,"properties":{"gw_eui":"FCC23DFFFE0AA6E3","gw_id":"goes"},"measurement_type_id":"snr"},{"value":-87,"properties":{"gw_eui":"FCC23DFFFE0B76E5","gw_id":"pz7kats"},"measurement_type_id":"rssi"},{"value":9.25,"properties":{"gw_eui":"FCC23DFFFE0B76E5","gw_id":"pz7kats"},"measurement_type_id":"snr"},{"value":-106,"properties":{"gw_eui":"FCC23DFFFE2EE75A","gw_id":"pz9stmaartensdijk"},"measurement_type_id":"rssi"},{"value":4.5,"properties":{"gw_eui":"FCC23DFFFE2EE75A","gw_id":"pz9stmaartensdijk"},"measurement_type_id":"snr"}],"payload":"bBECfOo="}`),
	},
	// {
	// 	input: []byte(``),
	// },
	// {
	// 	input: []byte(``),
	// },
	// {
	// 	input: []byte(``),
	// },
	// {
	// 	input: []byte(``),
	// },
	// {
	// 	input: []byte(``),
	// },
}
