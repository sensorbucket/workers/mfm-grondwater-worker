module sensorbucket.nl/workers/mfm-grondwater-worker

go 1.18

require github.com/sirupsen/logrus v1.8.1

require (
	github.com/pebbe/zmq4 v1.2.8
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
)
